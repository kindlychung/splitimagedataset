package lib

import (
	"io/ioutil"
	"fmt"
	"path/filepath"
	"math/rand"
	"log"
	"os"
)

type FolderSplitter struct {
	inputFolder     string
	outputFolder    string
	classes         []string
	trainRatio      float64
	validationRatio float64
}

func NewFolderSplitter(
	inputFolder string,
	outputFolder string,
	classes []string,
	testRatio float64,
	validationRatio float64,
) FolderSplitter {
	inputAbs := convertToAbsPath(inputFolder)
	outputAbs := convertToAbsPath(outputFolder)
	println(fmt.Sprintf("Input folder: %s\n", inputAbs))
	println(fmt.Sprintf("Output folder: %s\n", outputAbs))
	splitter := FolderSplitter{
		inputFolder:     inputAbs,
		outputFolder:    outputAbs,
		classes:         classes,
		trainRatio:      testRatio,
		validationRatio: validationRatio,
	}
	splitter.setClasses()
	return splitter
}

func convertToAbsPath(inputFolder string) string {
	var err error
	p, err := filepath.Abs(inputFolder)
	if err != nil {
		panic(fmt.Sprintf("Failed to expand abs path: %s", inputFolder))
	}
	return p
}

func (sf *FolderSplitter) setClasses() {
	inputEntriesInfo, err := ioutil.ReadDir(sf.inputFolder)
	if err != nil {
		panic(fmt.Sprintf("Failed to read directory: %s", sf.inputFolder))
	}
	sf.classes = make([]string, 0)
	for _, v := range inputEntriesInfo {
		if v.IsDir() {
			sf.classes = append(sf.classes, v.Name())
		}
	}
}

func (sf *FolderSplitter) GetClasses() []string {
	return sf.classes
}

func (sf *FolderSplitter) GetOutputFolder() string {
	return sf.outputFolder
}

func (sf *FolderSplitter) partitionSubFolder(partitionName string, className string) string {
	return filepath.Join(sf.GetOutputFolder(), partitionName, className)
}

func (sf *FolderSplitter) ShuffleFiles(className string) []string {
	classFolder := filepath.Join(sf.inputFolder, className)
	classFilesInfo, err := ioutil.ReadDir(classFolder)
	if err != nil {
		panic(fmt.Sprintf("Failed to read dir: %s", classFolder))
	}
	classFiles := make([]string, len(classFilesInfo))
	permutatedIndices := rand.Perm(len(classFilesInfo))
	for i, v := range permutatedIndices {
		classFiles[i] = filepath.Join(classFolder, classFilesInfo[v].Name())
	}
	return classFiles
}

func makeSplitIndices(length int, ratio1 float64, ratio2 float64) (int, int) {
	i1 := int(float64(length) * ratio1)
	i2 := int(float64(length) * ratio2)
	return i1, (i1 + i2)
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func copy(src string, dst string) {
	// Read all content of src to data
	data, err := ioutil.ReadFile(src)
	checkErr(err)
	// Write data to dst
	err = ioutil.WriteFile(dst, data, 0644)
	checkErr(err)
}

func printPercentage(finished int, total int, unit int) {
	if finished%unit == 0 {
		percent := float64(finished) / float64(total) * 100.0
		fmt.Printf("\rIn progress: %f%%", percent)
	} else if finished == (total - 1) {
		fmt.Printf("\rIn progress: %f%%", 100.0)
	}
}

func (sf *FolderSplitter) Partition() {
	os.RemoveAll(sf.outputFolder)
	for _, className := range sf.classes {
		fmt.Printf("Partitioning files for class: %s\n", className)
		classFiles := sf.ShuffleFiles(className)
		idx1, idx2 := makeSplitIndices(len(classFiles), sf.trainRatio, sf.validationRatio)
		trainFolder := sf.partitionSubFolder("train", className)
		validateFolder := sf.partitionSubFolder("validate", className)
		testFolder := sf.partitionSubFolder("test", className)
		os.MkdirAll(trainFolder, os.ModePerm)
		os.MkdirAll(validateFolder, os.ModePerm)
		os.MkdirAll(testFolder, os.ModePerm)
		for i := 0; i < idx1; i++ {
			printPercentage(i, len(classFiles), 10)
			src := classFiles[i]
			sourcBaseName := filepath.Base(src)
			dst := filepath.Join(trainFolder, sourcBaseName)
			copy(src, dst)
		}
		for i := idx1; i < idx2; i++ {
			printPercentage(i, len(classFiles), 10)
			src := classFiles[i]
			sourcBaseName := filepath.Base(src)
			dst := filepath.Join(validateFolder, sourcBaseName)
			copy(src, dst)
		}
		for i := idx2; i < len(classFiles); i++ {
			printPercentage(i, len(classFiles), 10)
			src := classFiles[i]
			sourcBaseName := filepath.Base(src)
			dst := filepath.Join(testFolder, sourcBaseName)
			copy(src, dst)
		}
		println()
	}
}
