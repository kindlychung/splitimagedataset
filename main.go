package main

import (
	"github.com/spf13/cobra"
	"bitbucket.org/kindlychung/splitimagedataset/cmd"
)

func main() {
	// set name of executable
	var rootCmd = &cobra.Command{Use: "splitImageDataSet"}
	rootCmd.AddCommand(cmd.PartitionCommand)
	rootCmd.Execute()
}
